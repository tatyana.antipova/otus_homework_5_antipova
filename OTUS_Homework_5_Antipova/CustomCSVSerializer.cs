﻿using System.Reflection;
using System.Text;

namespace OTUS_Homework_5_Antipova;

internal class CustomCSVSerializer
{
    private static string LIST_SEPARATOR = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;

    public static string Serialize<T>(object input)
    {
        Type type = typeof(T);
        StringBuilder sb = new StringBuilder();

        foreach (var field in type.GetFields(BindingFlags.Instance | BindingFlags.Public))
        {
            sb.Append(field.Name);
            sb.Append(LIST_SEPARATOR);
            sb.Append(field.GetValue(input));
            sb.Append(Environment.NewLine);
        }

        return sb.ToString();
    }

    public static object Deserialize<T>(string input)
    {
        string[] lines = input.Split(Environment.NewLine);
        Type type = typeof(T);

        object result = Activator.CreateInstance(type);

        foreach (var line in lines.Where(l => !string.IsNullOrWhiteSpace(l)))
        {
            string[] parts = line.Split(LIST_SEPARATOR);

            FieldInfo fieldInfo = type.GetField(parts[0], BindingFlags.Instance | BindingFlags.Public);
            
            fieldInfo.SetValue(obj: result, value: Convert.ChangeType(parts[1], fieldInfo.FieldType));
        }

        return result;
    }
}
