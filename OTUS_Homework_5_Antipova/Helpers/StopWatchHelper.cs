﻿using System.Diagnostics;

namespace OTUS_Homework_5_Antipova.Helpers;

public static class StopWatchHelper
{
    public static long MeasureTime(this Stopwatch sw, Action action, int iterations = 1)
    {
        sw.Reset();
        sw.Start();
        for (int i = 0; i < iterations; i++)
        {
            action();
        }
        sw.Stop();

        return sw.ElapsedMilliseconds;
    }
}
