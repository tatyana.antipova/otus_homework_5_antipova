﻿using BenchmarkDotNet.Attributes;
using Newtonsoft.Json;
using System.Text.Json;

namespace OTUS_Homework_5_Antipova.Helpers;

/*
|            Method |       Mean |    Error |   StdDev |
|------------------ |-----------:|---------:|---------:|
|     CustomExample | 1,363.8 ns | 23.01 ns | 21.52 ns |
| NewtonsoftExample | 2,109.2 ns | 37.55 ns | 35.12 ns |
| SystemTextExample |   288.7 ns |  5.76 ns | 11.90 ns |
*/


public class Benchmarks
{
    [Benchmark]
    public void CustomExample()
    {
        var testObject = F.Get();

        var csv = CustomCSVSerializer.Serialize<F>(testObject);
        var deserializedObject = CustomCSVSerializer.Deserialize<F>(csv);
    }

    [Benchmark]
    public void NewtonsoftExample()
    {
        var testObject = F.Get();

        var json = JsonConvert.SerializeObject(testObject);
        var deserializedObject = JsonConvert.DeserializeObject<F>(json);
    }

    [Benchmark]
    public void SystemTextExample()
    {
        var testObject = F.Get();

        var json = System.Text.Json.JsonSerializer.Serialize(testObject);
        var deserializedObject = System.Text.Json.JsonSerializer.Deserialize<F>(json);
    }
}
