﻿using BenchmarkDotNet.Running;
using Newtonsoft.Json;
using OTUS_Homework_5_Antipova.Helpers;
using System.Diagnostics;

namespace OTUS_Homework_5_Antipova;

internal class Program
{
    static void Main(string[] args)
    {
        //BenchmarkRunner.Run<Benchmarks>();

        var s = new Stopwatch();
        var testItem = F.Get();

        Console.WriteLine($"Сериализация собственная {s.MeasureTime(() => CustomCSVSerializer.Serialize<F>(testItem), 100_000)} мс"); /// Visual studio 2022, 100_000 итераций, 65 мс
        Console.WriteLine($"Сериализация NewtonSoft {s.MeasureTime(() => JsonConvert.SerializeObject(testItem), 100_000)} мс"); /// Visual studio 2022, 100_000 итераций, 333 мс

        Console.WriteLine($"Сколько времени потребовалось на вывод на консль {s.MeasureTime(() => Console.Write(""), 100_000)} мс"); /// Visual studio 2022, 100_000 итераций, 9 мс

        var csv = CustomCSVSerializer.Serialize<F>(testItem);
        var json = JsonConvert.SerializeObject(testItem);
        Console.WriteLine($"Десериализация собственная {s.MeasureTime(() => CustomCSVSerializer.Deserialize<F>(csv), 100_000)} мс"); /// Visual studio 2022, 100_000 итераций, 112 мс
        Console.WriteLine($"Десериализация NewtonSoft {s.MeasureTime(() => JsonConvert.DeserializeObject<F>(json), 100_000)} мс"); /// Visual studio 2022, 100_000 итераций, 252 мс

        Console.ReadLine();
    }
}